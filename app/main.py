"""
Main Python to run FastAPI
"""

from typing import Optional
from fastapi import FastAPI
app = FastAPI()

@app.get("/")
def read_root():
    """
    read_root
    """
    return {"Hello": "World"}

@app.get("/items/{item_id}")
def read_item(item_id: int, q_item: Optional[str] = None):
    """
    read_item
    """
    return {"item_id": item_id, "q_item": q_item}
