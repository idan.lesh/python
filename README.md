# python



## Questions

1. Develop docker image (Linux based) containing the following requirements:
1.1. Docker image for Python3 application based on FastAPI framework
1.1.1. Install the following python packages:
• Pandas
• Numpy
• Requests
2. Create new GitLab repository (private)
3. Create CI/CD process including python unit testing, code linting and image storage management.
4. Suggest a way to pull the repository from different machines without a git user.
5. Design (don’t implement!) an architecture for:
5.1. Automate the repository “pulling” activity whenever an update has been pushed to the repository on multiple machines.
5.2. Monitor docker containers on those machines
5.3. Design a mechanism which each developer won’t be able to push changes into “MAIN” branch without code review
5.4. Suggest a design for CI/CD automation by environments (staging / production)

## Answers

2. https://gitlab.com/idan.lesh/python

3. https://gitlab.com/idan.lesh/python/-/pipelines

4. We can use ssh key to pull/clone the repository. User that want to clone it, need to create ssh-key on his personal machine, copy the public key from file ~/.ssh/id_rsa.pub and add it to the Gitlab site: https://gitlab.com/-/profile/keys

5.
5.1. I know way to auto pull new code from repo using any IDE, for example IntelliJ. We can define the pull to run every X time (seconds, minutes).
5.2. 
5.3. Need to prevent commits directly to main branch. After every push to private branch, user can create PR. Every PR need to pass below conditions:
	a. Follow code review with more than 1 team member.
	b. Pass CI process on the branch.
5.4. Reg CI, there is no difference between ENVs as we are just building our code and create our artifacts. We need to make sure that one of our artifacts will be an Helm Chart. In Helm Chart we can maintain various templates (K8S resources) with multiple values files. We can create the below values files per ENV type:
	a. values.yaml (default for all Dev ENVs)
	b. valuesStaging.yaml (For staging ENVs)
	c. valuesProd.yaml (For production ENV)
	Assume we are using ArgoCD as our deploy tool, in the CD job, we will ask user to choose target ENV (Dev, Staging, Prod). Job itself, will create our application from the Helm chart and will apply the corresponding values file based on user choice.